-----------------------------------------------------------------------------------------------
-- Client Lua Script for TaxiMute
-- Copyright (c) NCsoft. All rights reserved
-----------------------------------------------------------------------------------------------
 
require "Window"
 
-----------------------------------------------------------------------------------------------
-- TaxiMute Module Definition
-----------------------------------------------------------------------------------------------
local TaxiMute = {} 
local voiceMuteConfig = "sound.volumeVoice"
local maxTaxiWindowCloseCount = 6

-----------------------------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------------------------
 
-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function TaxiMute:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self 

    return o
end

function TaxiMute:Init()
   local bHasConfigureFunction = false
   local strConfigureButtonText = ""
   local tDependencies = {}

   Apollo.RegisterAddon(self, bHasConfigureFunction, strConfigureButtonText, tDependencies)
end
 

-----------------------------------------------------------------------------------------------
-- TaxiMute OnLoad
-----------------------------------------------------------------------------------------------
function TaxiMute:OnLoad()
	self.currentCount = 0
	self.saveVolume = nil
	
	Apollo.RegisterEventHandler("TaxiWindowClose", "OnTaxiWindowClose", self)
	Apollo.RegisterTimerHandler("TaxiMute_TaxiCheckTimer", "OnCheckTimer", self)
	Apollo.RegisterTimerHandler("TaxiMute_OnTaxiTimer", "OnTaxiTimer", self)
end

-- It takes the game some time to transition between the close window and
-- actually being in the taxi.
function TaxiMute:OnTaxiWindowClose()
	self.currentCount = 0
	Apollo.CreateTimer("TaxiMute_TaxiCheckTimer", 0.5, false)
end

-- Sound configuration is an Account level variable. 
-- Thus we need to check the taxi state on an Account wide
-- basis rather than per Character.
function TaxiMute:OnRestore(eLevel, tData)
   if eLevel == GameLib.CodeEnumAddonSaveLevel.Account then
      self.saveVolume = tData.saveVolume
      self:UnmuteVolume()

      -- We may not be in the game world yet so kick off the window close timer logic
      -- to make sure we get into the game world
      self:OnTaxiWindowClose()
   end
end

function TaxiMute:OnSave(eLevel)
   if eLevel == GameLib.CodeEnumAddonSaveLevel.Account then
      local tData = {}
      tData.saveVolume = self.saveVolume
      return tData
   end

   return nil
end

-----------------------------------------------------------------------------------------------
-- TaxiMute Functions
-----------------------------------------------------------------------------------------------
-- Repeat for a max of 3s to see if we are on or off the taxi
function TaxiMute:OnCheckTimer()
	if self.saveVolume == nil and self.currentCount < maxTaxiWindowCloseCount then
		self.currentCount = self.currentCount + 1
		self:CheckTaxiState()
		Apollo.CreateTimer("TaxiMute_TaxiCheckTimer", 0.5, false)
	end
end

-- We are on the taxi! Check to make sure we have not gotten off the taxi.
function TaxiMute:OnTaxiTimer()
	if self.saveVolume ~= nil then
		self:CheckTaxiState()
		Apollo.CreateTimer("TaxiMute_OnTaxiTimer", 0.5, false)
	end
end

function TaxiMute:CheckTaxiState()
	local tmpOnTaxi = GameLib.GetPlayerTaxiUnit() ~= nil
	
	if tmpOnTaxi == false and self.saveVolume ~= nil then
		self:UnmuteVolume()
	elseif tmpOnTaxi and self.saveVolume == nil then
		self:MuteVolume()
	end
end

function TaxiMute:MuteVolume()
	if self.saveVolume == nil then
	   self.saveVolume = Apollo.GetConsoleVariable(voiceMuteConfig )

	   Apollo.SetConsoleVariable(voiceMuteConfig , 0)
	   Apollo.CreateTimer("TaxiMute_OnTaxiTimer", 0.5, false)

	   --Print("TaxiMute: Mute taxi driver.")
	end
end

function TaxiMute:UnmuteVolume()
   if self.saveVolume ~= nil then
      Apollo.SetConsoleVariable(voiceMuteConfig , self.saveVolume)
      self.saveVolume = nil
      --Print("TaxiMute: Unmute taxi driver.")
   end
end

-----------------------------------------------------------------------------------------------
-- TaxiMute Instance
-----------------------------------------------------------------------------------------------
local TaxiMuteInst = TaxiMute:new()
TaxiMuteInst:Init()
